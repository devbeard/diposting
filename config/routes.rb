# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
Rails.application.routes.draw do

  root to:"main#index"

  # for authentication
  get "sign-up", to: "registration#new"
  post "sign-up", to: "registration#create"

  delete "logout", to: "session#destroy"
  get "sign-in", to: "session#new"
  post "sign-in", to: "session#create"

  # change password
  get "password", to: "password#edit", as: :edit_password
  patch "password", to: "password#update", as: :update_password

  # password reset
  get "password/reset", to: "password_reset#new"
  post "password/reset", to: "password_reset#create"

  # for about page
  get "about", to: "about#index"
  # for features page
  get "features", to: "features#index"
  # for faq page
  get "faq", to: "faq#index"
  # for pricing page
  get "pricing", to: "pricing#index"

end
